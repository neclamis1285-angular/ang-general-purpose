/**
 * Created by Bernard on 12/1/2015.
 */
'use strict';

(function (generalPurposeModule) {

	generalPurposeModule.service('generalPurpose', [
		'$rootScope', '$http', '$mdDialog', 'moment',
		function ($rootScope, $http, $mdDialog) {

			return {

				// Add CSS Rule
				addCssRule: function (sheet, selector, rules, index) {

					if ('insertRule' in sheet) {
						sheet.insertRule(selector + '{' + rules + '}', index);
					} else if ('addRule' in sheet) {
						sheet.addRule(selector, rules, index);
					}

				},

				// Capitalize First Word In String
				capitalizeFirstWord: function (value) {

					if (value) {
						return value.charAt(0).toUpperCase() + value.slice(1);
					}

					return value;

				},

				// Check Is Function Is Empty Or Null
				isFunctionEmpty: function (functionToTest) {

					if (functionToTest == null) {
						return true;
					}
					return (Object.prototype.toString.call(functionToTest).indexOf(
						"function") > -1);

				},

				// Check If Object Is Empty Or Null
				isObjectEmpty: function (object) {

					if (object == null) {
						return true;
					}
					return Object.keys(object).length === 0;

				},

				// Check If String Is Empty Or Null
				isStringEmpty: function (value) {

					if (value == null || value.trim() == '') {
						return true;
					}
					return false;

				},

				// Check Link For Protocol
				checkLinkForProtocol: function (link) {

					var startingUrl = 'http://';
					var httpsStartingUrl = 'https://';
					return !!(link.startWith(startingUrl) || link.startWith(httpsStartingUrl));

				},

				// Check Password Requirements
				checkPasswordRequirements: function (password) {

					var anUpperCase = /[A-Z]/;
					var aLowerCase = /[a-z]/;
					var aNumber = /[0-9]/;
					var obj = {};
					obj.result = true;

					if (password.length < 9) {
						obj.result = false;
						obj.error = "Password Requirement Not Met!";
						return obj;
					}

					var numUpper = 0;
					var numLower = 0;
					var numNums = 0;
					for (var i = 0; i < password.length; i++) {
						if (anUpperCase.test(password[i]))
							numUpper++;
						else if (aLowerCase.test(password[i]))
							numLower++;
						else if (aNumber.test(password[i]))
							numNums++;
					}

					if (numUpper < 1 || numLower < 1 || numNums < 1) {
						obj.result = false;
						obj.error = "Password Requirement Not Met!";
						return obj;
					}
					return obj;

				},

				// Concatenate Object Property
				concatenateObjectProperty: function (string, object, property,
					addHtmlBreak) {

					if (object[property] != undefined) {
						if (addHtmlBreak) {
							return string += object[property] + '<br>';
						} else {
							return string += object[property] + ' ';
						}
					} else {
						return string;
					}

				},

				// Concatenate String With Separator
				concatenateStringWithSeparator: function () {

					/*
					 * To use this function, the first parameter needs to be your separator
					 */

					var separator = arguments[0];
					var returnVal = '';

					for (var i = 1; i < arguments.length; i++) {
						var currentArgument = arguments[i];

						if (!this.isStringEmpty(currentArgument)) {
							returnVal += currentArgument + separator;
						}
					}

					return this.trimString(returnVal, separator);

				},

				convertUtcToLocal: function (utcDateTime, utcDateTimeFormat,
					localDateTimeFormat) {

					return moment.utc(utcDateTime, utcDateTimeFormat).local().format(
						localDateTimeFormat);

				},

				// Create Style Sheet
				createStyleSheet: function (mode) {

					var style = document.createElement('style');

					style.setAttribute('media', mode ? mode : 'all');
					style.appendChild(document.createTextNode(''));
					document.head.appendChild(style);

					return style.sheet;

				},

				// Disable File Caching
				disableFileCaching: function (fileName) {

					if (fileName) {
						return fileName + '?' + Date.now();
					}

					return fileName;

				},

				// Format Phone Number
				formatPhoneNumber: function (phoneNumber) {

					phoneNumber = phoneNumber.replace(/(\d{3})(\d{3})(\d{4})/,
						"($1) $2-$3");
					return phoneNumber;

				},

				// Get Base URL
				getBaseUrl: function () {

					var url = location.href;
					var baseURL = url.substring(0, url.indexOf('/', 14));

					if (baseURL.indexOf('http://localhost') != -1) {
						// Base Url for localhost
						var url = location.href; // window.location.href;
						var pathname = location.pathname; // window.location.pathname;
						var index1 = url.indexOf(pathname);
						var index2 = url.indexOf("/", index1 + 1);
						var baseLocalUrl = url.substr(0, index2);

						return baseLocalUrl + "/";
					} else {
						return baseURL + "/";
					}

				},

				// Get Element CSS Property
				getElementCssProperty: function (elementId, propertyName) {

					var element = angular.element(document).find(elementId);

					return element.css(propertyName);

				},

				// Get Hours
				getHours: function () {

					var hr = 0;
					var hours = [];

					for (var i = 0; i <= 24; i++) {
						var newHour = {
							title: hr++
						};
						hours.push(newHour);
					}
					return hours;

				},

				// Get Minutes
				getMinutes: function () {

					var min = 0;
					var minutes = [];

					for (var i = 0; i < 60; i++) {
						var newMin = {
							title: min++
						};
						minutes.push(newMin);
					}
					return minutes;

				},

				// Get Months
				getMonths: function () {

					var months = [{
						title: 'January',
						value: 1
					}, {
						title: 'February',
						value: 2
					}, {
						title: 'March',
						value: 3
					}, {
						title: 'April',
						value: 4
					}, {
						title: 'May',
						value: 5
					}, {
						title: 'June',
						value: 6
					}, {
						title: 'July',
						value: 7
					}, {
						title: 'August',
						value: 8
					}, {
						title: 'September',
						value: 9
					}, {
						title: 'October',
						value: 10
					}, {
						title: 'November',
						value: 11
					}, {
						title: 'December',
						value: 12
					}];
					return angular.copy(months, []);

				},

				// Get Screen Resolution
				getScreenResolution: function () {

					var resolution = {
						height: window.screen.availHeight,
						width: window.screen.availWidth
					};

					return resolution;

				},

				// Get Status'
				getStatus: function () {

					var status = [{
						title: 'Active',
						value: true
					}, {
						title: 'InActive',
						value: false
					}];

					return angular.copy(status, []);

				},

				// Generate Time Increments
				generateTimeIncrements: function (incrementMinutes) {

					var timeArray = [];
					var hours, minutes, amPm;
					for (var i = 0; i <= (1440 - incrementMinutes); i += incrementMinutes) {
						hours = Math.floor(i / 60);

						minutes = i % 60;
						if (minutes < 10) {
							minutes = '0' + minutes;
						}
						amPm = hours % 24 < 12 ? 'AM' : 'PM';

						hours = hours % 12;
						if (hours === 0) {
							hours = 12;
						}

						timeArray.push(hours + ':' + minutes + ' ' + amPm);
					}

					return timeArray;

				},

				// Get File Extension
				getFileExtension: function (filename) {

					filename = filename.replace('url("', '').replace('")', '').replace('url(', '').replace(');', '').replace(')', '');
					var retValue = (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : undefined;
					var queryPosition = retValue.indexOf('?');
					if (queryPosition > -1) {
						retValue = retValue.substring(0, queryPosition);
					}

					return retValue;

				},

				// Get URL Query String Property
				getQueryStringProperty: function (key) {

					var vars = [];
					var hash = null;
					var url = window.location.href;
					var queryStart = url.indexOf('?') + 1;
					var queryEnd = url.length + 1;
					var hashIndex = window.location.href.indexOf('#');
					var hashes = url.slice(queryStart, (hashIndex > 0 && hashIndex >
							queryStart) ? hashIndex :
						queryEnd).split('&');

					for (var i = 0; i < hashes.length; i++) {
						hash = hashes[i].split('=');
						vars.push(hash[0].toLowerCase());
						if (hash[0] && hash[1]) {
							vars[hash[0].toLowerCase()] = hash[1].toLowerCase();
						}
					}

					return vars[key.toLowerCase()];

				},

				// Get User Has Role
				getUserHasRole: function (roleName) {

					var userRoles = this.getUserRoles();
					if (userRoles != undefined) {
						for (var i = 0; i < userRoles.length; i++) {
							var currentRole = userRoles[i];

							if (currentRole == roleName) {
								return true;
							}
						}
					}
					return false;

				},

				// Get US State Abbreviations
				getStates: function () {

					var states = [{
						title: 'AL'
					}, {
						title: 'AK'
					}, {
						title: 'AZ'
					}, {
						title: 'AR'
					}, {
						title: 'CA'
					}, {
						title: 'CO'
					}, {
						title: 'CT'
					}, {
						title: 'DE'
					}, {
						title: 'DC'
					}, {
						title: 'FL'
					}, {
						title: 'GA'
					}, {
						title: 'HI'
					}, {
						title: 'ID'
					}, {
						title: 'IL'
					}, {
						title: 'IN'
					}, {
						title: 'IA'
					}, {
						title: 'KS'
					}, {
						title: 'KY'
					}, {
						title: 'LA'
					}, {
						title: 'ME'
					}, {
						title: 'MD'
					}, {
						title: 'MA'
					}, {
						title: 'MI'
					}, {
						title: 'MN'
					}, {
						title: 'MS'
					}, {
						title: 'MO'
					}, {
						title: 'MT'
					}, {
						title: 'NE'
					}, {
						title: 'NV'
					}, {
						title: 'NH'
					}, {
						title: 'NJ'
					}, {
						title: 'NM'
					}, {
						title: 'NY'
					}, {
						title: 'NC'
					}, {
						title: 'ND'
					}, {
						title: 'OH'
					}, {
						title: 'OK'
					}, {
						title: 'OR'
					}, {
						title: 'PA'
					}, {
						title: 'RI'
					}, {
						title: 'SC'
					}, {
						title: 'SD'
					}, {
						title: 'TN'
					}, {
						title: 'TX'
					}, {
						title: 'UT'
					}, {
						title: 'VT'
					}, {
						title: 'VA'
					}, {
						title: 'WA'
					}, {
						title: 'WV'
					}, {
						title: 'WI'
					}, {
						title: 'WY'
					}];
					return angular.copy(states, []);

				},

				// Get Years
				getYears: function (fromYear, endYear) {

					var startYear = parseInt(fromYear);
					var stopYear = parseInt(endYear);
					var yearDiff = stopYear - startYear;
					var years = [];

					for (var i = 0; i < yearDiff; i++) {
						var newYear = {
							title: startYear++
						};
						years.push(newYear);
					}
					return years;

				},

				// Pad Number With Leading Zeros
				padNumber: function pad(string, numberOfDigits) {

					string = string.toString();
					if (string.indexOf('.') > -1) {
						var periodPosition = string.indexOf('.');
						var leftNum = string.substring(1, periodPosition);
						var rightNum = string.substring(periodPosition, string.length);
						string = leftNum;
						string = string.toString();
						var numPadded = string.length < numberOfDigits ? pad("0" + string,
							numberOfDigits) : string;
						return numPadded + rightNum;
					} else {
						return string.length < numberOfDigits ? pad("0" + string,
							numberOfDigits) : string;
					}

				},

				// Round Number To Decimal Places
				roundNumberToDecimalPlaces: function (number, decimalPlaces) {

					var returnValue = number;

					if (!isNaN(number)) {
						var multiplier = 1;

						for (var i = 0; i < decimalPlaces; i++) {
							multiplier = multiplier + '' + 0;
						}

						returnValue = Math.round(number * multiplier) / multiplier;
					}

					return returnValue;

				},

				// Set Element CSS Property
				setElementCssProperty: function (elementId, propertyName, propertyValue) {

					var element = angular.element(document).find(elementId);
					element.css(propertyName, propertyValue);

				},

				// Set Interval And Run Immediately
				setIntervalImmediate: function (seconds, callback) {

					callback();

					return setInterval(callback, seconds * 1000);

				},

				// Show Alert
				showAlert: function ($targetEvent, title, message) {

					var parentEl = angular.element(document).find('body');

					var alert = $mdDialog.alert({
						parent: parentEl,
						templateUrl: 'view/main/alertDialog.html',
						targetEvent: $targetEvent,
						clickOutsideToClose: true,
						locals: {
							alertTitle: title,
							alertMessage: message,
							closeDialog: function () {
								$mdDialog.hide();
								alert = undefined;
							}
						},
						controllerAs: 'ctrl'
					});

					$mdDialog
						.show(alert)
						.finally(function () {
							alert = undefined;
						});

				},

				// Trim String
				trimString: function (string, whatToTrim) {

					var whatToTrimLength = whatToTrim.length;
					var leftTrim = string.substr(0, whatToTrimLength);
					var rightTrim = string.substr(string.length - whatToTrimLength, string
						.length);

					if (leftTrim == whatToTrim) {
						string = string.substr(0, whatToTrimLength);
					}

					if (rightTrim == whatToTrim) {
						string = string.substr(0, string.length - whatToTrimLength);
					}

					return string;

				},

				// Truncate String
				truncateString: function (str, length, ending) {

					if (length == null) {
						return str;
					}
					if (ending == null) {
						ending = '...';
					}
					if (str.length > length) {
						return str.substring(0, length - ending.length) + ending;
					} else {
						return str;
					}

				},

				// Validate Email Address
				validateEmailAddress: function (emailAddress) {

					var re =
						/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
					return re.test(emailAddress);

				},

				// Validate Link
				validateLink: function (link) {

					$http({
						method: 'GET',
						url: link
					}).then(function successCallback(response) {
						if (response && response.status == 404) {
							if (callback) {
								callback(false);
							}
						} else {
							if (callback) {
								callback(true);
							}
						}
					}, function errorCallback(response) {
						if (callback) {
							callback(false);
						}
					});

				},

				// Validate Object Values
				validateObjectValues: function (objectToValidate) {

					if (!objectToValidate) {
						return false;
					}

					for (var key in objectToValidate) {
						var value = objectToValidate[key];

						if (value) {
							if (Array.isArray(value) || typeof value == 'object') {
								this.validateObjectValues(value);
							}
						} else {
							return false;
						}
					}

					return true;

				}

			}

		}
	]);

	// region Filters

	// Convert A Null Value To 0
	generalPurposeModule.filter('nullZero', function () {

		return function (value) {
			return value == null ? 0 : value;
		};

	});

	// Convert Active String To Boolean True/False
	generalPurposeModule.filter('activeStatusInverse', function () {

		return function (status) {
			return status == 'Active';
		};

	});

	// Convert An Array To String By An Object Property
	generalPurposeModule.filter('arrayToStringByProperty', function () {

		return function (array, property, prefix) {

			if (array != undefined && array.length > 0 && property != undefined) {
				var retVal = '';

				for (var i = 0; i < array.length; i++) {
					var currentVal = array[i];
					retVal += (prefix || '') + _.get(currentVal, property) + ', ';
				}

				return retVal.trimString(', ');
			} else if (array != undefined && array.length > 0 && property ==
				undefined) {
				var retVal = '';

				for (var i = 0; i < array.length; i++) {
					var currentVal = array[i];
					retVal += (prefix || '') + currentVal + ', ';
				}

				return retVal.trimString(', ');
			} else {
				return 'N/A';
			}

		};

	});

	// Convert An Status ID To A Pre-Defined Text (Online/Offline/In Progress)
	generalPurposeModule.filter('statusIdToText', function () {

		return function (value) {
			if (value != undefined) {
				if (value == 1) {
					return 'Online';
				} else if (value == 2) {
					return 'Offline';
				} else if (value == 4) {
					return 'In Progress';
				}
			}
		};

	});

	// Convert Boolean To Active/InActive String
	generalPurposeModule.filter('activeStatus', function () {

		return function (status) {
			return status ? 'Active' : 'InActive';
		};

	});

	// Convert Boolean To String Representation
	generalPurposeModule.filter('boolToString', function () {

		return function (boolean, stringTrue, stringFalse) {
			return boolean ? stringTrue : stringFalse;
		};

	});

	// Convert UTC DateTime To Local DateTime
	generalPurposeModule.filter('convertUtcToLocal', function () {

		return function (utcDateTime, utcDateTimeFormat, localDateTimeFormat) {
			return moment.utc(utcDateTime, utcDateTimeFormat).local().format(
				localDateTimeFormat);
		};

	});

	// Check Link For Protocol
	generalPurposeModule.filter('linkProtocol', function () {

		return function (link) {

			var result;
			var startingUrl = "http://";
			var httpsStartingUrl = "https://";
			if (link.startWith(startingUrl) || link.startWith(httpsStartingUrl)) {
				result = link;
			} else {
				result = startingUrl + link;
			}

			return result;
		}

	});

	// Filter By Date Range
	generalPurposeModule.filter('dateRange', function () {

		return function (items, startDate, endDate, dateProperty) {

			var retArray = [];

			if (!startDate && !endDate) {
				return items;
			}

			angular.forEach(items, function (obj) {
				var receivedDate = moment(new Date(obj[dateProperty]));
				var startDateMatch = receivedDate.isSameOrAfter(startDate, 'day');
				var endDateMatch = receivedDate.isSameOrBefore(endDate, 'day');

				if (startDateMatch && endDateMatch) {
					retArray.push(obj);
				}
			});

			return retArray;

		}

	});

	// Format Date
	generalPurposeModule.filter('formatDate', function () {

		return function (date, format) {
			return moment(new Date(date)).format(format);
		};

	});

	// Format Phone
	generalPurposeModule.filter('phoneFormat', function () {

		var phoneFormat = /^(\d{3})?(\d{3})(\d{4})$/;

		return function (input) {
			var parsed = phoneFormat.exec(input);

			return (!parsed) ? input : ((parsed[1]) ? '(' + parsed[1] + ') ' : '') +
				parsed[2] + '-' + parsed[3];
		}

	});

	// Sum An Array Of Numbers
	generalPurposeModule.filter('arraySum', function () {

		return function (array, property) {

			if (array != undefined && property != undefined) {
				var sum = 0;

				for (var i = 0; i < array.length; i++) {
					var val = parseFloat(array[i][property]);

					sum += val;
				}

				return sum;
			} else if (array != undefined && property == undefined) {
				var sum = 0;

				for (var i = 0; i < array.length; i++) {
					var val = parseFloat(array[i]);

					sum += val;
				}

				return sum;
			}

		};

	});

	// endregion

})(angular.module("generalPurpose", []));

// region Prototype

// Check If String Start With Supplied String
String.prototype.startWith = function (str) {

	return this.indexOf(str) == 0;

};

// Trim SubString From String
String.prototype.trimString = function (string) {

	if (string != undefined) {
		var trimmedString = this;
		var startChars, endChars;

		// Trim Beginning Of String
		startChars = this.indexOf(string);
		if (startChars == 0) {
			trimmedString = this.substring(string.length, this.length - string.length);
		}

		// Trim Ending Of String
		endChars = trimmedString.lastIndexOf(string);
		if (endChars > -1 && (endChars + string.length == trimmedString.length)) {
			trimmedString = trimmedString.slice(0, endChars);
		}

		return trimmedString;
	}

};

Element.prototype.getElementHeight = function () {

	if (typeof this.clip !== "undefined") {
		return this.clip.height;
	} else {
		if (this.style.pixelHeight) {
			return this.style.pixelHeight;
		} else {
			return this.offsetHeight;
		}
	}

};

Element.prototype.getElementWidth = function () {

	if (typeof this.clip !== "undefined") {
		return this.clip.width;
	} else {
		if (this.style.pixelWidth) {
			return this.style.pixelWidth;
		} else {
			return this.offsetWidth;
		}
	}

};

// endregion